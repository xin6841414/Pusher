<?php 

require_once './Arr.php';

use Swoole\Table as SwooleTable;

class Table
{
	protected $table;

	public function __construct()
	{
		$this->table = new SwooleTable(1024);
		$this->table->column('commander', SwooleTable::TYPE_INT);
		$this->table->column('soliders', SwooleTable::TYPE_STRING, 2048);
		$this->table->create();
		$this->table->set('maps', ['commander' => 0, 'soliders' => '']);
	}

	public function __set($field, $value)
	{
		$maps = $this->table->get('maps');

		if ($field == 'commander') {
			$maps[$field] = $value;		
		} else {
			$maps[$field] = (string) $value;
		}
		
		
		$this->table->set('maps', $maps);
	}

	public function __get($field)
	{
		$value = $this->table->get('maps', $field);

		if ($field == 'commander') {
			return $value;	
		} else {
			return new Arr(json_decode($value));
		}
		
	}

}