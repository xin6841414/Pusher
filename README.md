# Pusher

#### 项目介绍
利用Swoole进行项目更新命令推送, 便于更新多个服务器代码.

#### 软件架构
 - Server.php
 - Client.php
 - Table.php
 - Solider.php
 - Arr.php
 - Commander.html

#### 依赖

- swoole 2.0以上
- php 7.0以上

#### 安装教程

```shell
php Server.php
```

其中一台项目机器
```shell
php Solider.php
```

另一台项目机器 
```shell
php Solider.php
```

管理后台
```shell
php -S 0.0.0.0:9080
```

浏览器访问
http://xxx:9080/Commander.html

