<?php 

class Arr implements IteratorAggregate, ArrayAccess
{
	protected $arr;

	public function __construct($arr)
	{
		$this->arr = $arr;
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
            $this->arr[] = $value;
        } else {
            $this->arr[$offset] = $value;
        }
	}

	public function offsetExists($offset) {
        return isset($this->arr[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->arr[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->arr[$offset]) ? $this->arr[$offset] : null;
    }

	public function getIterator()
	{
		return new ArrayIterator($this->arr);
	}

	public function __toString()
	{
		return json_encode($this->arr);
	}
}